# Twitter Analysis in Spanish  
## Goals  
1. Improve current sentiment analysis vocabulary in Spanish.  
2. Create a report able to give useful insights from Twitter, containing: 1) Followers by party, 2) Party trends, 3) Segmentation, and 4) Maps of the followers by party.  
3. Write a report on sentiment analysis.  
4. The created reports must follow the European data protection policies.  
5. Creating a structure able to detect irony.  

## Steps  
1. Collect data of followers from Twitter accounts.  
2. Create a map and data base of the places where the followers live.  
3. Identify possible Twitter bots. 
4. Conduct a sentiment analysis of the tweets.  
One of the importan features of this last step would be to create an "irony detector". The first idea is that, being *irony* ["the use of words to express something other than an especially the opposite of the literal meaning"](https://www.merriam-webster.com/dictionary/irony), it becomes important knowing who the sender is. Is this person for or against the receiver? A logic on the matter can be developed so the combination of sender, sentimental degree of the message, and receiver could give us some results that can be assessed to know how useful they are.  
The logic of the irony:

|sender|receiver|identification|message|sentiment                      |  
|------|--------|--------------|-------|-------------------------------|
|1     |1       | 1            |1      |Support                        |
|1     |1       | 1            |0      |Constructive critic            |
|1     |0       | 0            |1      |Irony or sincere congratulation|
|1     |0       | 0            |0      |Attack                         |
  
  
Although there is a lot of shades and possible combinations, we simplify the subject in four cases: 

1. Identificaction and positive message: sender and receiver belongs to the same spectrum of ideas and recognize themselves as such. If, under this circumnstances the message is positive, we call it "a supportive message".  
2. Identification and negative message: In this case,sender and receiver also belongs to the same spectrum, but the sender does not agree with the receiver. Therefore, the sender tries not to be aggresive with the receiver. Therefore the message is what colloquially is known as "a constructive critic".  
3. There is no identification and the message is possitive: In this case an "ironic" message is likely. However, it is also possible to consider that, the sender would have perceived a previous change in the receiver position and, therefore, congratulates him.  
4. There is no identification and the message is negative: Is an attack.  

This is not a definitve classification and must be considered a provisional guide with an exploratory character.  


## Reached Goals  
1. Until now, it has been possible to get the number of followers and its changes across time.  
2. Segmentation: three groups has been identified: 1) Follower of all the studied accounts, 2) Followers of left leaned parties, and 3) followers of the not left leaned parties.  
3. However, another segment was created: those who follow party 1 and 2, but do not follow party 3 (all inside the same political group). Note that to delimitate this segment the rules were different from those followed in the prevoious step.  
4. We have improved the stopwords in Spanish using words that appears in the tweets.  




  